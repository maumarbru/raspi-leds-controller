import time
from adapter import Adapter, _rgb_to_int as rgb

a = Adapter()

def sb(b=1.5, s=0.15):
    a.clear()
    a[4] = rgb(25,20,0)
    for ii in range(110):
        a[snake_boom[ii//10]] = rgb(0,int(b**(ii%10)),0)
        time.sleep(s)
    a[snake_boom[ii//10]] = rgb(50,0,0)

snake_boom = [0,7,8,15,14,13,12,11,4,5,6,5,5,5,5,5]

for ii in reversed(range(10)):
    s = ii/300
    a.strip.setBrightness(255)
    sb(1.4, s)
    for brightness in reversed(range(22)):
        a.strip.setBrightness(int(1.3**brightness))
        a.strip.show()
        time.sleep(0.016)
    a.strip.setBrightness(255)
