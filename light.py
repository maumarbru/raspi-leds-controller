from adapter import CircularMirrorAdapter, Color

WHITE = Color(1, 0.8, 0.6)
WARM = Color(1, 0.6, 0.2)

def main():
    mirror = CircularMirrorAdapter()
    mirror.brightness = 0.3
    mirror[:] = [WARM for _ in mirror.led_angles]


if __name__ == '__main__':
    main()
