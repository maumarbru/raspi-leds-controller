from adapter import CircularMirrorAdapter, Color

BLACK = Color(0, 0, 0)

def main():
    mirror = CircularMirrorAdapter()
    mirror[:] = [BLACK for _ in mirror.led_angles]


if __name__ == '__main__':
    main()
