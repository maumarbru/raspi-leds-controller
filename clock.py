import os
import time
from contextlib import suppress
from datetime import datetime
from adapter import CircularMirrorAdapter, positive_fraction, Color
from postmethod import piper

SMOOTH = os.getenv('SMOOTH', False)


def main():
    mirror = CircularMirrorAdapter()
    mirror | loop(SMOOTH)


@piper
def loop(mirror, smooth=False):
    with suppress(KeyboardInterrupt):
        while True:
            mirror | render_clock
            time.sleep(1/25 if smooth else _to_next_half_second())


def _to_next_half_second():
    t = time.time()
    sec_frac = t - int(t)
    delay = 1.5 - sec_frac
    return delay


@piper
def render_clock(mirror):
    now = datetime.now()
    bright_time = 9 <= now.hour < 22
    mirror.brightness = 1 if bright_time else 0.03
    mirror[:] = [
        normal_angle
        | change_origin_and_mirror
        | time_to_color(just_hour=not bright_time, portion=3/60)
        | (invert_dark if bright_time else identity)
        for normal_angle in mirror.led_angles
    ]


@piper
def identity(obj):
    return obj


@piper
def invert_dark(color):
    return 0.5|white if color == Color(0, 0, 0) else color


@piper
def white(intencity):
    f = intencity
    return Color(1*f, 0.8*f, 0.6*f)


@piper
def warm(intencity):
    f = intencity
    return Color(1*f, 0.7*f, 0.3*f)


@piper
def change_origin_and_mirror(normal_angle):
    return positive_fraction(0.25 - normal_angle)


@piper
def hms(d: datetime, smooth=SMOOTH):
    h, m = d.hour + d.minute / 60 + d.second / 3600, d.minute + d.second / 60
    s = d.second + d.microsecond / 1e6 if smooth else d.second
    return h, m, s


@piper
def time_to_color(clock_fraction, just_hour=False, portion=2/60):
    h, m, s = datetime.now() | hms
    hour_fraction = positive_fraction(h / 12)
    minute_fraction = positive_fraction(m / 60)
    second_fraction = positive_fraction(s / 60)
    proximity = proximity_function(clock_fraction, portion=portion)
    if just_hour:
        return Color(
            hour_fraction | proximity,
            0,
            0,
        )
    else:
        return Color(
            hour_fraction | proximity,
            minute_fraction | proximity,
            second_fraction | proximity,
        )


@piper
def proximity_function(pos0, pos1, portion):
    assert portion > 0, "portion must be larger than zero."
    factor = 1 / portion
    distance = abs_distance(pos0, pos1)  # [0, 0.5]
    normal_proximity = 2 * (0.5 - distance)
    proximity = normal_proximity * factor - (factor - 1)
    return proximity if proximity >= 0 else 0


def abs_distance(f0, f1):
    d = positive_fraction(f0 - f1)
    return d if d <= 0.5 else 1 - d


if __name__ == '__main__':
    main()
