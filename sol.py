"""
000 111 0   
0   1 1 0   
  0 1 1 0   
000 111 000 
"""

import time
from adapter import Adapter4x4, Color
from postmethod import piper

d = 1 
PALETTE = {
    ' ': Color(0.0, 0.0, 0.0),
    '0': Color(1.0, 0.2, 0.0),
    '1': Color(1.0, 0.7, 0.0),
}

PIXEL_MAP = __doc__[1:-1].splitlines()


@piper
def show(fb, blocks_gen, interval_s=1, repeat_n=10):
    for _ in range(repeat_n):
        for block in blocks_gen(PIXEL_MAP):
            copy(fb, block)
            time.sleep(interval_s)
        fb.clear()
        time.sleep(interval_s)


def non_overlaped_blocks(pixel_map):
    for ii in range(0, 4*5, 4):
        yield [row[ii:ii+4] | padding | palette_to_color for row in pixel_map]


@piper
def palette_to_color(row):
    return [PALETTE[c] for c in row]


@piper
def padding(row, cols=4):
    n = len(row)
    return row if n >= cols else row + ' ' * (cols - n)


def copy(fb, block):
    with fb:
        for r in range(4):
            for c in range(4):
                fb[r, c] = block[r][c]


def scroll_image(pixel_map):
    for ii in range(0, 4*6):
        yield [left_blank(row)[ii:ii+4] | padding | palette_to_color for row in pixel_map]


def left_blank(row):
    return '    ' + row


def sub_pixel_scroll_image(pixel_map):
    pixel_map = [
        left_blank(row) | padding(cols=24) | palette_to_color
        for row in pixel_map
    ]
    x = 0
    while True:
        if x >= 20:
            break
        yield [
            [
                row[int(ii + x)] * (1 - frac(x)) + row[int(ii + x + 1)] * frac(x)
                for ii in range(4)
            ]
            for row in pixel_map
        ]
        x += 0.1


def frac(value):
    return value - int(value)


if __name__ == '__main__':
    fb = Adapter4x4()
    try:
        fb | show(non_overlaped_blocks, interval_s=0.5, repeat_n=2)
        fb | show(scroll_image, interval_s=0.2, repeat_n=2)
        fb | show(sub_pixel_scroll_image, interval_s=1/60, repeat_n=2)
    finally:
        fb.clear()
