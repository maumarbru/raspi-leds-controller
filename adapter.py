from math import fmod
from collections import namedtuple
from rpi_ws281x import Adafruit_NeoPixel, Color as _rgb_to_int


class Color(namedtuple('Color', 'red,green,blue')):
    def __str__(self):
        return '#' + hex(int(self))[2:]

    def __int__(self):
        return _rgb_to_int(*map(_color_channel_to_int, self))

    def __add__(self, other: 'Color'):
        return Color(*(a + b for a, b in zip(self, other)))

    def __mul__(self, factor: float):
        return Color(*(c * factor for c in self))


def _color_channel_to_int(float_color):
    n = round(float_color**2 * 255)
    if n < 0:
        return 0
    elif n > 255:
        return 255
    else:
        return int(n)


def ensure_iterable(value_or_iterable):
    try:
        yield from value_or_iterable
    except TypeError:
        while True:
            yield value_or_iterable


class Adapter:
    LED_COUNT = 16        # Number of LED pixels.
    LED_PIN = 18          # GPIO pin connected to the pixels (must support PWM!).
    LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA = 10          # DMA channel to use for generating signal (try 10)
    LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
    LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)

    def __init__(self, strip: Adafruit_NeoPixel = None):
        self.strip = strip or self.create_default_Adafruit_NeoPixel()
        self.strip.begin()
        self._update = True

    def __setitem__(self, index, color: Color):
        if isinstance(index, slice):
            for ii, one_color in zip(range(*index.indices(len(self))), ensure_iterable(color)):
                self.strip.setPixelColor(ii, int(one_color))
        else:
            self.strip.setPixelColor(index, int(color))
        if self._update:
            self.strip.show()

    def __getitem__(self, index) -> int:
        return self.strip.getPixelColor(index)

    @classmethod
    def create_default_Adafruit_NeoPixel(cls):
        return Adafruit_NeoPixel(
            cls.LED_COUNT,
            cls.LED_PIN,
            cls.LED_FREQ_HZ,
            cls.LED_DMA,
            cls.LED_INVERT,
            cls.LED_BRIGHTNESS,
        )

    def __enter__(self):
        self._update = False
        return self

    def __exit__(self, *args):
        self._update = True
        self.strip.show()

    def __len__(self):
        return self.strip.numPixels()

    def clear(self):
        with self:
            for ii in range(len(self)):
                self[ii] = Color(0, 0, 0)

    @property
    def brightness(self):
        return self.strip.getBrightness()

    @brightness.setter
    def brightness(self, value):
        self.strip.setBrightness(round(value * 255))


class Adapter4x4(Adapter):
    MAP = [
        [ 0, 1, 2, 3],
        [ 7, 6, 5, 4],
        [ 8, 9,10,11],
        [15,14,13,12],
    ]
    LED_COUNT = sum(len(row) for row in MAP)

    def __setitem__(self, index, color: Color):
        super().__setitem__(self._linear_index(index), color)

    def __getitem__(self, index) -> int:
        return super().__getitem__(self._linear_index(index), color)

    def _linear_index(self, index):
        try:
            row, col = index
        except TypeError:
            return index
        else:
            return self.MAP[row][col]


class CircularMirrorAdapter(Adapter):
    LED_COUNT = 83
    LED_0_ANGLE = 0.75  # [rad / 2π]

    @property
    def led_angles(self):
        '''Return a list with the normalized angle in radians/2π [0, 1) for each LED.'''
        return [
            positive_fraction(ii / self.LED_COUNT + self.LED_0_ANGLE)
            for ii in range(self.LED_COUNT)
        ]


def positive_fraction(value):
    fraction = fmod(value, 1)
    return fraction if fraction >= 0 else fraction + 1
