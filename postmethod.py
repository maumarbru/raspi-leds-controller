class piper:
    def __init__(self, func, *args, **kwargs):  # Decorator
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.__doc__ = func.__doc__

    def __call__(self, *args, **kwargs):  # Partial
        return self.__class__(self.func, *args, **kwargs)

    def __ror__(self, instance):  # Executor
        return self.func(instance, *self.args, **self.kwargs)

    def __repr__(self):
        return "<piper form for {func.__name__!r} with args={args} kwargs={kwargs}>" \
               .format(**vars(self))

