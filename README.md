# Raspi LEDs Controller


## Examples
```sh
sudo python3 lucup.py
```

```sh
sudo python3 sol.py
```

## References

- https://tutorials-raspberrypi.com/connect-control-raspberry-pi-ws2812-rgb-led-strips/
- https://github.com/richardghirst/rpi_ws281x
- https://github.com/rpi-ws281x/rpi-ws281x-python

